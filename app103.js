// outer function
function greet() {

    const name = 'Mr.John Doe'
    // inner function
    function displayName(){
        console.log("Hi: " + name)
    }
    return displayName

}

const g1 = greet()
console.log(g1)
console.log(g1())