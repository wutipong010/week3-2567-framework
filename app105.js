// Pure function
// Side effect
const c = 10
function add(a,b){
    return a + b + c;
}

const result = add(1,6)
console.log("Pure function: ",result)