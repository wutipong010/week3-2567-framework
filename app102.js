function a() {
    console.log("Callback Function")
}
// Mested function
function sayHi(callback,fname, lname) {
    callback();
    function getFullName(){
        return fname +" " + lname
    }
    return getFullName()
}

const message = sayHi(a,"Mr.Mark","Zuckerburg");
console.log(message)